# pythagoras-utils *1.0.0*

> A javascript module with utility functions and classes that are used throughout the pythagoras applications.


### src/Parse.js


#### parsePhoneNumber() 








##### Returns


- `Void`



#### new Parse() 

This class provides methods for parsing user input to an application-wide
valid format.






##### Returns


- `Void`



#### Parse.phone(string) 

Parses a phone number.




##### Parameters

| Name | Type | Description |  |
| ---- | ---- | ----------- | -------- |
| string | `String`  | - The phone string to parse. | &nbsp; |




##### Returns


- `String`  Returns the parsed phone number or null if it is invalid.




### src/Validate.js


#### parsePhoneNumber() 








##### Returns


- `Void`



#### new Validate() 

The Validate class can be used to validate data input by a user.






##### Returns


- `Void`



#### Validate.email(string) 

Validates whether string is an email address.




##### Parameters

| Name | Type | Description |  |
| ---- | ---- | ----------- | -------- |
| string | `String`  | - The email string to validate. | &nbsp; |




##### Returns


- `Boolean`  Whether the string is a valid email address.



#### Validate.name(string) 

Validates whether string is a valid name.




##### Parameters

| Name | Type | Description |  |
| ---- | ---- | ----------- | -------- |
| string | `String`  | - The name string to validate. | &nbsp; |




##### Returns


- `Boolean`  Whether the string is a valid name.



#### Validate.password(string) 

Validates whether string is a valid password.




##### Parameters

| Name | Type | Description |  |
| ---- | ---- | ----------- | -------- |
| string | `String`  | - The password string to validate. | &nbsp; |




##### Returns


- `Boolean`  Whether the string is a valid password.



#### Validate.phone(string) 

Validates whether string is a valid phone number.




##### Parameters

| Name | Type | Description |  |
| ---- | ---- | ----------- | -------- |
| string | `String`  | - The phone string to validate. | &nbsp; |




##### Returns


- `Boolean`  Whether the string is a valid phone number.



#### Validate.uuid(string) 

Validates whether string is a valid UUID (v4).




##### Parameters

| Name | Type | Description |  |
| ---- | ---- | ----------- | -------- |
| string |  | - The UUID string to validate. | &nbsp; |




##### Returns


- `Boolean`  Whether the string is a valid UUID.




### src/accessEnv.js


#### cache() 








##### Returns


- `Void`



#### accessEnv(key, defaultValue) 

Accesses a variable inside of process.env throwing an error if not found.
Caches variables.




##### Parameters

| Name | Type | Description |  |
| ---- | ---- | ----------- | -------- |
| key | `string`  | - Environment variable name. | &nbsp; |
| defaultValue |  | - Value to return if not found. If this value is not  set, an error is thrown if the environment variable is not found. | &nbsp; |




##### Returns


- `Void`




### src/extendClass.js


#### extendClass(base, sub) 

Sets up the prototype chain for a sub class inheriting from a base class.




##### Parameters

| Name | Type | Description |  |
| ---- | ---- | ----------- | -------- |
| base |  | - The base class to be inherited from. | &nbsp; |
| sub |  | - The sub class that inherits from the base class. | &nbsp; |




##### Returns


- `Void`




### src/index.js


#### module.exports() 








##### Returns


- `Void`




### src/logger.js


#### winston() 








##### Returns


- `Void`



#### logger() 

An instantiation of the winston logger.






##### Returns


- `Void`




### src/errors/APIError.js


#### new APIError() 

The APIError can be used to return any application-error in a fixed format.






##### Returns


- `Void`



#### APIError.constructor(statusCode, message, param2) 






##### Parameters

| Name | Type | Description |  |
| ---- | ---- | ----------- | -------- |
| statusCode | `Number`  | - A HTTP status code that describes the error. | &nbsp; |
| message | `String`  | - A message that specifies the error. | &nbsp; |
| param2 | `OptionalErrorAttributes`  |  | &nbsp; |




##### Returns


- `Void`



#### APIError.response() 

Builds a response object that can be returned to the client..






##### Returns


- `Object`  The response object.



#### APIError.expressResponse(res) 






##### Parameters

| Name | Type | Description |  |
| ---- | ---- | ----------- | -------- |
| res | `Object`  | - The express response. | &nbsp; |




##### Returns


- `Object`  The modified express response object.



#### APIError.getExpressResponse(res) 

Creates a response for epxress framework controllers.




##### Parameters

| Name | Type | Description |  |
| ---- | ---- | ----------- | -------- |
| res | `Object`  | - The express response. | &nbsp; |




##### Returns


- `Object`  The modified express response object.



#### APIError.setup(param0) 

Sets up the APIError class and all child error classes.




##### Parameters

| Name | Type | Description |  |
| ---- | ---- | ----------- | -------- |
| param0 | `Object`  | - Options passed to the setup function. | &nbsp; |
| setup.source | `String`  | - Sets the source field for all error objects. | &nbsp; |




##### Returns


- `Void`



#### APIError.source() 

The source application from which the APIError originated.






##### Returns


- `Void`




### src/errors/AuthenticationError.js


#### APIError() 








##### Returns


- `Void`



#### new AuthenticationError() 

The AuthenticationError class can be thrown if authentication was required
but not provided (HTTP 401).






##### Returns


- `Void`



#### AuthenticationError.constructor([message&#x3D;&quot;Accessing], param1) 






##### Parameters

| Name | Type | Description |  |
| ---- | ---- | ----------- | -------- |
| message&#x3D;&quot;Accessing | `String`  | this endpoint requires authentication."] | *Optional* |
| param1 | `OptionalErrorAttributes`  |  | &nbsp; |




##### Returns


- `Void`




### src/errors/ForbiddenError.js


#### APIError() 








##### Returns


- `Void`



#### new ForbiddenError() 

The ForbiddenError class can be thrown if the server refuses action due to
the user not having the necessary permissions for a resource or needing an
account of some sort, or attempting a prohibited action (HTTP 403).






##### Returns


- `Void`



#### ForbiddenError.constructor([message&#x3D;&quot;Unauthorized], param1) 






##### Parameters

| Name | Type | Description |  |
| ---- | ---- | ----------- | -------- |
| message&#x3D;&quot;Unauthorized | `String`  | to access this endpoint."] | *Optional* |
| param1 | `OptionalErrorAttributes`  |  | &nbsp; |




##### Returns


- `Void`




### src/errors/InputError.js


#### APIError() 








##### Returns


- `Void`



#### new InputError() 

The InputError class can be thrown if the request was well-formed but was
unable to be followed due to semantic errors (HTTP 422).






##### Returns


- `Void`



#### InputError.constructor([message&#x3D;&quot;The], param1) 






##### Parameters

| Name | Type | Description |  |
| ---- | ---- | ----------- | -------- |
| message&#x3D;&quot;The | `String`  | provided input is invalid."] | *Optional* |
| param1 | `OptionalErrorAttributes`  |  | &nbsp; |




##### Returns


- `Void`




### src/errors/NotFoundError.js


#### APIError() 








##### Returns


- `Void`



#### new NotFoundError() 

The NotFoundError class can be thrown if the requested resource could not be
found but may be available in the future (HTTP 404).






##### Returns


- `Void`



#### NotFoundError.constructor([message&#x3D;&quot;The], param1) 






##### Parameters

| Name | Type | Description |  |
| ---- | ---- | ----------- | -------- |
| message&#x3D;&quot;The | `String`  | requested endpoint does not exist."] | *Optional* |
| param1 | `OptionalErrorAttributes`  |  | &nbsp; |




##### Returns


- `Void`




### src/errors/ServerError.js


#### APIError() 








##### Returns


- `Void`



#### new ServerError() 

The ServerError class can be thrown as a generic error given when an
unexpected condition was encountered and no more specific message is
suitable (HTTP 500).






##### Returns


- `Void`



#### ServerError.constructor([message&#x3D;&quot;An], param1) 






##### Parameters

| Name | Type | Description |  |
| ---- | ---- | ----------- | -------- |
| message&#x3D;&quot;An | `String`  | unknown error has occurred."] | *Optional* |
| param1 | `OptionalErrorAttributes`  |  | &nbsp; |




##### Returns


- `Void`




### src/errors/TimeoutError.js


#### APIError() 








##### Returns


- `Void`



#### new TimeoutError() 

The TimeoutError class can be thrown if the user has sent too many requests
in a given amount of time (HTTP 429).






##### Returns


- `Void`



#### TimeoutError.constructor([message&#x3D;&quot;The], param1) 






##### Parameters

| Name | Type | Description |  |
| ---- | ---- | ----------- | -------- |
| message&#x3D;&quot;The | `String`  | request has timed out."] | *Optional* |
| param1 | `OptionalErrorAttributes`  |  | &nbsp; |




##### Returns


- `Void`




### src/errors/ValidationError.js


#### APIError() 








##### Returns


- `Void`



#### new ValidationError() 

The ValidationError class can be thrown if the server cannot or will not
process the request due to an apparent client error (e.g., malformed request
syntax, size too large, invalid request message framing, or deceptive
request routing) (HTTP 400).






##### Returns


- `Void`



#### ValidationError.constructor([message&#x3D;&quot;The], param1) 






##### Parameters

| Name | Type | Description |  |
| ---- | ---- | ----------- | -------- |
| message&#x3D;&quot;The | `String`  | provided data is invalid."] | *Optional* |
| param1 | `OptionalErrorAttributes`  |  | &nbsp; |




##### Returns


- `Void`




### src/errors/expressErrorMiddleware.js


#### APIError() 








##### Returns


- `Void`



#### expressErrorMiddleware(err, req, res, next) 

Middleware for the express node.js framework for handling pythagoras service
errors.




##### Parameters

| Name | Type | Description |  |
| ---- | ---- | ----------- | -------- |
| err | `Object`  | - The error object to be handled. | &nbsp; |
| req | `Object`  | - The express request object. | &nbsp; |
| res | `Object`  | - The express response object. | &nbsp; |
| next | `Function`  | - The express callback for executing the next route. | &nbsp; |




##### Returns


- `Object`  The modified express response object.




### src/errors/index.js


#### module.exports() 








##### Returns


- `Void`




*Documentation generated with [doxdox](https://github.com/neogeek/doxdox).*
