/**
 * Language CEFR levels.
 *
 * @type {Array<string>}
 */
const languageLevelList = ["A1", "A2", "B1", "B2", "C1", "C2"];

module.exports = { languageLevelList };
