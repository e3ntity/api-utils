/**
 * @fileoverview This file exports all data.
 * @author Lukas Schneider
 */

module.exports = {
  ...require("./countries"),
  ...require("./countryToCurrency"),
  ...require("./languageLevels"),
  ...require("./languages"),
  ...require("./university"),
};
