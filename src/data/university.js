/**
 * @typedef University
 *   One country.
 * @property {string} emailDomain
 *   University email domain.
 * @property {string} name
 *   University name.
 * @property {string} country
 *   University country as ISO 3166-1 alpha-3 code.
 */

/**
 * List of universities.
 *
 * @type {Array<University>}
 */
const universityList = [
  // { emailDomain: "caltech.edu", name: "California Institute of Technology", country: "USA" },
  // { emailDomain: "harvard.edu", name: "Harvard University", country: "USA" },
  { emailDomain: "imperial.ac.uk", name: "Imperial College London", country: "GBR" },
  // { emailDomain: "mit.edu", name: "Massachusetts Institute of Technology", country: "USA" },
  // { emailDomain: "stanford.edu", name: "Stanford University", country: "USA" },
  { emailDomain: "ethz.ch", name: "Swiss Federal Institute of Technology", country: "CHE" },
  { emailDomain: "cam.ac.uk", name: "University of Cambridge", country: "GBR" },
  // { emailDomain: "uchicago.edu", name: "University of Chicago", country: "USA" },
  // { emailDomain: "ed.ac.uk", name: "University of Edinburgh", country: "GBR" },
  { emailDomain: "ox.ac.uk", name: "University of Oxford", country: "GBR" },
  { emailDomain: "ucl.ac.uk", name: "UCL", country: "GBR" },
];

const isUniversityEmail = (email) =>
  universityList.find((university) => email.toLowerCase().endsWith(university.emailDomain));
const getUniversityFromEmail = (email) =>
  universityList.find((university) => email.toLowerCase().endsWith(university.emailDomain));

module.exports = { getUniversityFromEmail, isUniversityEmail, universityList };
