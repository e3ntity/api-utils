/**
 * @fileoverview This file defines the ForbiddenError class.
 * @author Lukas Schneider
 */

const APIError = require("./APIError");

/**
 * The ForbiddenError class can be thrown if the server refuses action due to
 * the user not having the necessary permissions for a resource or needing an
 * account of some sort, or attempting a prohibited action (HTTP 403).
 * 
 * @extends {APIError.APIError}
 * @memberof APIError
 * @version 1.1.0
 */
class ForbiddenError extends APIError {
  /**
   * @param {String} [message="Unauthorized to access this endpoint."]
   * @param {OptionalErrorAttributes} param1
   */
  constructor(
    message = "Unauthorized to access this endpoint.",
    { code } = {}
  ) {
    super(403, message, { code });
    this.name = "ForbiddenError";
  }
}

module.exports = ForbiddenError;
