/**
 * @fileoverview This file defines the NotFoundError class.
 * @author Lukas Schneider
 */

const APIError = require("./APIError");

/**
 * The NotFoundError class can be thrown if the requested resource could not be
 * found but may be available in the future (HTTP 404).
 * 
 * @extends {APIError.APIError}
 * @memberof APIError
 * @version 1.1.0
 */
class NotFoundError extends APIError {
  /**
   * @param {String} [message="The requested endpoint does not exist."]
   * @param {OptionalErrorAttributes} param1
   */
  constructor(
    message = "The requested endpoint does not exist.",
    { code, fields } = {})
  {
    super(404, message, { code, fields });
    this.name = "NotFoundError";
  }
}

module.exports = NotFoundError;
