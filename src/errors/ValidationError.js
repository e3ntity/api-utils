/**
 * @fileoverview This file defines the ValidationError class.
 * @author Lukas Schneider
 */

const APIError = require("./APIError");

/**
 * The ValidationError class can be thrown if the server cannot or will not
 * process the request due to an apparent client error (e.g., malformed request
 * syntax, size too large, invalid request message framing, or deceptive
 * request routing) (HTTP 400).
 * 
 * @extends {APIError.APIError}
 * @memberof APIError
 * @version 1.1.0
 */
class ValidationError extends APIError {
  /**
   * @param {String} [message="The provided data is invalid."]
   * @param {OptionalErrorAttributes} param1
   */
  constructor(
    message = "The provided data is invalid.",
    { code, fields } = {}
  ) {
    super(400, message, { code, fields });
    this.name = "ValidationError";
  }
}

module.exports = ValidationError;
