/**
 * @fileoverview This file defines the ServerError class.
 * @author Lukas Schneider
 */

const APIError = require("./APIError");

/**
 * The ServerError class can be thrown as a generic error given when an
 * unexpected condition was encountered and no more specific message is
 * suitable (HTTP 500).
 * 
 * @extends {APIError.APIError}
 * @memberof APIError
 * @version 1.1.0
 */
class ServerError extends APIError {
  /**
   * @param {String} [message="An unknown error has occurred."]
   * @param {OptionalErrorAttributes} param1
   */
  constructor(message = "An unknown error has occurred.", { code } = {}) {
    super(500, message, { code });
    this.name = "ServerError";
  }
}

module.exports = ServerError;
