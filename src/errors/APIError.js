/**
 * @fileoverview This file defines the base APIError class.
 * @author Lukas Schneider
 */

/**
 * Optional attributes to be passed along with the error.
 *
 * @typedef {Object} OptionalErrorAttributes
 * @property {Number} code - A code that uniquely identifies the error within
 *  the application.
 * @property {String[]} fields - Specifies which input fields were faulty. May
 *  be used with HTTP codes 400, 404 and 422.
 * @property {String} [source=APIError.source] - The source this error
 *  originated from.
 * @property {Number} timeout - Specifies a timeout that the front-end  needs
 *  to wait before repeating the request. May be used with HTTP code 429.
 */

/**
 * The APIError can be used to return any application-error in a fixed format.
 *
 * @memberof APIError
 * @version 1.1.0
 */
class APIError extends Error {
  /**
   * @param {Number} statusCode - A HTTP status code that describes the error.
   * @param {String} message - A message that specifies the error.
   * @param {OptionalErrorAttributes} param2

   */
  constructor(statusCode, message, { code, fields, source = APIError.source, timeout } = {}) {
    super(message);
    this.name = "APIError";

    this.statusCode = statusCode;

    this.code = code;
    this.error = true;
    this.fields = fields;
    this.source = source;
    this.timeout = timeout;
  }

  /**
   * Builds a response object that can be returned to the client..
   *
   * @deprecated Getter functions should not be used according to the [AirBnB
   *  style guide](https://github.com/airbnb/javascript), which this module
   *  follows. Use {@link APIError#getResponse} instead.
   * @returns {Object} The response object.
   */
  get response() {
    return this.getResponse();
  }

  getResponse() {
    let data = {
      error: true,
      message: this.message,
      source: this.source,
      statusCode: this.statusCode,
    };

    if (this.code !== undefined) data = { ...data, code: this.code };

    if (this.fields !== undefined) data = { ...data, fields: this.fields.sort() };

    if (this.timeout !== undefined) data = { ...data, timeout: this.timeout };

    return data;
  }

  /**
   * @deprecated Use {@link APIError#getExpressResponse} instead.
   * @param {Object} res - The express response.
   * @returns {Object} The modified express response object.
   */
  expressResponse(res) {
    return this.getExpressResponse(res);
  }

  /**
   * Creates a response for epxress framework controllers.
   *
   * @param {Object} res - The express response.
   * @returns {Object} The modified express response object.
   */
  getExpressResponse(res) {
    return res.status(this.statusCode).json(this.getResponse());
  }

  /**
   * Sets up the APIError class and all child error classes.
   *
   * @param {Object} param0 - Options passed to the setup function.
   * @param {String} setup.source - Sets the source field for all error objects.
   */
  static setup({ source = "unknown" }) {
    APIError.source = source;
  }

  /**
   * Creates an APIError from a axios error response.
   *
   * @param {Error} axiosError - The axios error object
   * @returns {APIError} An api error extracted from the axios error.
   */
  static fromAxiosError(axiosError) {
    // Unknown error
    if (!axiosError.response || !axiosError.response.data) return new APIError(500, "An unknown error has occurred.");

    const { source, statusCode, message, ...rest } = axiosError.response.data;

    // Unknown network error
    if (!source || !statusCode || !message) return new APIError(500, "An unknown error has occurred.");

    return new APIError(statusCode, message, { source, ...rest });
  }
}

/**
 * The source application from which the APIError originated.
 * @type {String}
 * @static
 */
APIError.source = "unknown";

module.exports = APIError;
