/**
 * @fileoverview This file defines the AuthenticationError class.
 * @author Lukas Schneider
 */

const APIError = require("./APIError");

/**
 * The AuthenticationError class can be thrown if authentication was required
 * but not provided (HTTP 401).
 * 
 * @extends {APIError.APIError}
 * @memberof APIError
 * @version 1.1.0
 */
class AuthenticationError extends APIError {
  /**
   * @param {String} [message="Accessing this endpoint requires authentication."]
   * @param {OptionalErrorAttributes} param1
   */
  constructor(
    message = "Accessing this endpoint requires authentication.",
    { code } = {}
  ) {
    super(401, message, { code });
    this.name = "AuthenticationError";
  }
}

module.exports = AuthenticationError;
