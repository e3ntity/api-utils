/**
 * @fileoverview This file defines the expressErrorMiddleware function.
 * @author Lukas Schneider
 */

const APIError = require("./APIError");
const ServerError = require("./ServerError");

/**
 * Sets up the express error middleware function.
 * @param {Object} param0 - Arguments passed to the middleware.
 * @param {Function} logError - A function to use for logging errors.
 * @return {Function} Returns the express middleware for handling errors.
 */
const expressErrorMiddleware = ({ logError } = {}) => {

  /**
   * Middleware for the express node.js framework for handling pythagoras
   * service errors.
   * 
   * @param {Object} err - The error object to be handled.
   * @param {Object} req - The express request object.
   * @param {Object} res - The express response object.
   * @param {Function} next - The express callback for executing the next route.
   * @returns {Object} The modified express response object.
   * @memberof APIError
   * @version 1.0.0
   */
  return (err, req, res, next) => {
    // Error to be passed on to the user
    if (err instanceof APIError)
      return err.getExpressResponse(res);

    // Error from another service
    if (err.response && err.response.data && typeof err.response.data.source === "string") {
      const { statusCode, message, ...data } = err.response.data;
      
      const serviceError = new APIError(statusCode, message, data);

      return serviceError.getExpressResponse(res);
    }

    logError(err);

    return new ServerError().getExpressResponse(res);
  }
}

module.exports = expressErrorMiddleware;
