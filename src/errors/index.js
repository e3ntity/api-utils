/**
 * @fileoverview This file exports all error classes.
 * @author Lukas Schneider
 */

/**
 * The APIError namespace provides several classes for representing api errors.
 * Note: Use instanceof only for checking whether the error is an APIError, not
 * whether it is some sub-class.
 * @namespace APIError
 */

module.exports = {
  APIError: require("./APIError"),
  AuthenticationError: require("./AuthenticationError"),
  expressErrorMiddleware: require("./expressErrorMiddleware"),
  ForbiddenError: require("./ForbiddenError"),
  InputError: require("./InputError"),
  NotFoundError: require("./NotFoundError"),
  ServerError: require("./ServerError"),
  TimeoutError: require("./TimeoutError"),
  ValidationError: require("./ValidationError"),
};
