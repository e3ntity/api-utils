/**
 * @fileoverview This file defines the TimeoutError class.
 * @author Lukas Schneider
 */

const APIError = require("./APIError");

/**
 * The TimeoutError class can be thrown if the user has sent too many requests
 * in a given amount of time (HTTP 429).
 * 
 * @extends {APIError.APIError}
 * @memberof APIError
 * @version 1.1.0
 */
class TimeoutError extends APIError {
  /**
   * @param {String} [message="The request has timed out."]
   * @param {OptionalErrorAttributes} param1
   */
  constructor(message = "The request has timed out.", { code, timeout } = {}) {
    super(429, message, { code, timeout });
    this.name = "TimeoutError";
  }
}

module.exports = TimeoutError;
