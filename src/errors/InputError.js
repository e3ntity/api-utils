/**
 * @fileoverview This file defines the InputError class.
 * @author Lukas Schneider
 */

const APIError = require("./APIError");

/**
 * The InputError class can be thrown if the request was well-formed but was
 * unable to be followed due to semantic errors (HTTP 422).
 * 
 * @extends {APIError.APIError}
 * @memberof APIError
 * @version 1.1.0
 */
class InputError extends APIError {
  /**
   * @param {String} [message="The provided input is invalid."]
   * @param {OptionalErrorAttributes} param1
   */
  constructor(
    message = "The provided input is invalid.",
    { code, fields } = {}
  ) {
    super(422, message, { code, fields });
    this.name = "InputError";
  }
}

module.exports = InputError;
