/**
 * @fileoverview This file is the entry point to the utils module.
 * @author Lukas Schneider
 */

module.exports = {
  accessEnv: require("./accessEnv"),
  ...require("./data"),
  ...require("./errors"),
  extendClass: require("./extendClass"),
  Parse: require("./Parse"),
  Validate: require("./Validate"),
};
