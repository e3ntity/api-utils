/**
 * @fileoverview This file defines a utility function for extending a class by
 *  setting up the prototype chain.
 * @author Lukas Schneider
 */

/**
 * Sets up the prototype chain for a sub class inheriting from a base class.
 * @param {*} base - The base class to be inherited from.
 * @param {*} sub - The sub class that inherits from the base class.
 */
function extendClass(base, sub) {
  const originalPrototype = sub.prototype;

  sub.prototype = Object.create(base.prototype);

  for (let key in originalPrototype)
    sub.prototype[key] = originalPrototype[key];

  Object.defineProperty(sub.prototype, "constructor", {
    enumerable: false,
    value: sub,
  });
}

module.exports = extendClass;
