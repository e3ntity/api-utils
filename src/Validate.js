/**
 * @fileoverview This file defines the Validate class that can be used to
 *  validate user input to any service.
 * @author Lukas Schneider
 */

const parsePhoneNumber = require("libphonenumber-js");
const validator = require("validator");

const { countryList } = require("./data/countries");
const { languageLevelList } = require("./data/languageLevels");
const { languageList } = require("./data/languages");

/**
 * The Validate class can be used to validate data input by a user.
 * @version 1.0.0
 */
class Validate {
  static biographyMaxLength = 255;
  static biographyMinLength = 0;
  static cefrLevels = languageLevelList;
  static emailMaxLength = 255;
  static nameMaxLength = 255;
  static nameMinLength = 2;
  static passwordMaxLength = 255;
  static passwordMinLength = 8;
  static regexName = /^[A-Za-zÀ-ÖØ-öø-ÿ' ]+[A-Za-zÀ-ÖØ-öø-ÿ']$/;
  static iso6392Languages = languageList.map((language) => language.iso6392B);
  static iso31661Alpha3Countries = countryList.map((country) => country.alpha3);
  static locations = ["zürich"];

  /**
   * Validates whether string is a biography.
   * @param {String} string - The biography to validate.
   * @returns Whether the string is a valid biography.
   */
  static biography(string) {
    return (
      Validate.string(string) &&
      string.length >= Validate.biographyMinLength &&
      string.length <= Validate.biographyMaxLength
    );
  }

  /**
   * Validates whether value is a CEFR level code.
   * @param {String} value - The code to validate.
   * @returns {Boolean} Whether the value is a CEFR code.
   */
  static cefr(value) {
    return Validate.string(value) && Validate.cefrLevels.includes(value);
  }

  /**
   * Validates whether string is a ISO 3166-1 Alpha-3 country code.
   * @param {String} string - The country to validate.
   * @returns {Boolean} Whether the string is a valid country code.
   */
  static iso31661Alpha3(value) {
    return Validate.string(value) && Validate.iso31661Alpha3Countries.includes(value);
  }

  /**
   * Validates whether string is an email address.
   * @param {String} string - The email string to validate.
   * @returns {Boolean} Whether the string is a valid email address.
   */
  static email(string) {
    return Validate.string(string) && validator.isEmail(string) && string.length <= Validate.emailMaxLength;
  }

  /**
   * Validates whether value is a ISO 639-2 language code.
   * @param {String} value - The value to validate.
   * @returns {Boolean} Whether value is a ISO 639-2 language code.
   */
  static iso6392(value) {
    return Validate.string(value) && Validate.iso6392Languages.includes(value);
  }

  /**
   * Validates whether string is a valid location.
   * @param {String} string - The location to validate.
   * @returns {Boolean} Whether the string is a valid location.
   */
  static location(string) {
    return Validate.string(string) && Validate.locations.includes(string);
  }

  /**
   * Validates whether string is a valid name.
   * @param {String} string - The name string to validate.
   * @returns {Boolean} Whether the string is a valid name.
   */
  static name(string) {
    return (
      Validate.string(string) &&
      Validate.regexName.test(string) &&
      string.length >= Validate.nameMinLength &&
      string.length <= Validate.nameMaxLength
    );
  }

  /**
   * Validate whether value is a number.
   * @param {Number} value - The value to validate.
   * @returns {Boolean} Whether the value is a number.
   */
  static number(value) {
    return value && typeof value === "number";
  }

  /**
   * Validates whether string is a valid password.
   * @param {String} string - The password string to validate.
   * @returns {Boolean} Whether the string is a valid password.
   */
  static password(string) {
    return (
      Validate.string(string) &&
      string.length >= Validate.passwordMinLength &&
      string.length <= Validate.passwordMaxLength
    );
  }

  /**
   * Validate whether value is a percentage.
   * @param {Number} value - The value to validate.
   * @returns {Boolean} Whether the value is a percentage.
   */
  static percentage(value) {
    return Validate.number(value) && value >= 0 && value < 100;
  }

  /**
   * Validates whether string is a valid phone number.
   * @param {String} string - The phone string to validate.
   * @returns {Boolean} Whether the string is a valid phone number.
   */
  static phone(string) {
    if (!Validate.string(string)) return false;

    let phone;
    try {
      phone = parsePhoneNumber(string);
    } catch (err) {}

    return phone && phone.isValid();
  }

  /**
   * Validates whether string is a valid string.
   * @param {String} string - The string to validate.
   * @returns {Boolean} Whether the string is a string.
   */
  static string(string) {
    return string && typeof string === "string";
  }

  /**
   * Validates whether string is a valid UUID (v4).
   * @param {*} string - The UUID string to validate.
   * @returns {Boolean} Whether the string is a valid UUID.
   */
  static uuid(string) {
    return Validate.string(string) && validator.isUUID(string, 4);
  }
}

module.exports = Validate;
