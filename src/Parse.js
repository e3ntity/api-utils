/**
 * @fileoverview This file defines the Parse class that can be used to
 *  parse user input to any service.
 * @author Lukas Schneider
 */

const parsePhoneNumber = require("libphonenumber-js");

/**
 * This class provides methods for parsing user input to an application-wide
 * valid format.
 *
 * @version 1.0.0
 */
class Parse {
  /**
   * Parses a phone number.
   *
   * @param {String} string - The phone string to parse.
   * @returns {String} Returns the parsed phone number or null if it is invalid.
   */
  static phone(string) {
    let phone;
    try {
      phone = parsePhoneNumber(string);
    } catch (err) {}

    if (!phone || !phone.isValid()) return null;

    return phone.number.replace(/\D/g, "");
  }
}

module.exports = Parse;
