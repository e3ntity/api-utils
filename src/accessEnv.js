/**
 * @fileoverview This file contains a helper function for accessing environment
 *  variables.
 * @author Lukas Schneider
 */

const cache = {};

const handleAccessError =
  process.env.NODE_ENV === "test"
    ? (_) => {}
    : (key) => {
        throw new Error(`${key} not found in process.env!`);
      };

/**
 * Accesses a variable inside of process.env throwing an error if not found.
 * Caches variables.
 * @param {string} key - Environment variable name.
 * @param {*} defaultValue - Value to return if not found. If this value is not
 *  set, an error is thrown if the environment variable is not found.
 */
const accessEnv = (key, defaultValue) => {
  if (!(key in process.env)) {
    if (defaultValue !== undefined) return defaultValue;
    handleAccessError(key);
  }

  if (cache[key]) return cache[key];

  cache[key] = process.env[key];

  return process.env[key];
};

module.exports = accessEnv;
